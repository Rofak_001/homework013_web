import React from "react";
import ItemTable from "./Components/ItemTable";
import ItemNavbar from "./Components/ItemNavbar";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Add from "./Components/Pages/Add";
import View from "./Components/Pages/View";
function App() {
  return (
    <div>
      <BrowserRouter>
      <ItemNavbar/>
        <Switch>
          <Route exact path="/" component={ItemTable}/> 
          <Route path="/add" component={Add}/>
          <Route path="/view/:id" component={View}/>
          <Route path="/*" render={()=><h3>Page Not found</h3>}/>
        </Switch>
      </BrowserRouter>
    </div>
  );
}
export default App;

