import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Table, Container, Button } from "react-bootstrap";
import Axios from "axios";
import ItemRowTable from "./ItemRowTable";
import { Link } from "react-router-dom";
import ItemModal from "./ItemModal";
import Pagination from "react-js-pagination";
import "../Components/css/style.css"
export default class ItemTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      article: [],
      showModal: false,
      message: "",
      data: {
        DATA: [],
      },
      activePage: 1,
      perPage: 5,
    };
  }
  //get data from api
  componentWillMount() {
    Axios.get(
      "http://110.74.194.124:15011/v1/api/articles?page=1&limit=15"
    ).then((res) => {
      console.log(res.data.DATA);
      this.setState({ article: res.data.DATA });
    });
  }
  // delete data
  onDelete = (deleteID) => {
    let article = this.state.article.filter((data) => data.ID !== deleteID);
    this.setState({
      article: article,
      showModal: true,
    });
    Axios.delete(
      `http://110.74.194.124:15011/v1/api/articles/${deleteID}`
    ).then((res) => {
      console.log("asdfsadf" + res.data.MESSAGE);
      // alert(res.data.MESSAGE);
      this.setState({
        message: res.data.MESSAGE,
      });
    });
    // Axios.get(
    //   "http://110.74.194.124:15011/v1/api/articles?page=1&limit=15"
    // ).then((res) => {
    //   console.log(res.data.DATA);
    //   this.setState({ article: res.data.DATA });
    // });
  };
  // componentWillUpdate(){
  //       Axios.get(
  //     "http://110.74.194.124:15011/v1/api/articles?page=1&limit=15"
  //   ).then((res) => {
  //     console.log(res.data.DATA);
  //     this.setState({ article: res.data.DATA });
  //   });
  // }
  closeModal = () => {
    this.setState({ showModal: false });
  };
  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  }
  render() {
    const indexOfLast = this.state.activePage * this.state.perPage;
    const indexOfFirst = indexOfLast - this.state.perPage;
    const current = this.state.article.slice(indexOfFirst, indexOfLast);
    let tr = current.map((value, index) => (
      <ItemRowTable key={index} article={value} onDelete={this.onDelete} />
    ));
    return (
      <Container>
        <div className="text-center mt-3">
          <h3>Article Managment</h3>
          <Button variant="dark" as={Link} to="/add">
            Add New Article
          </Button>
        </div>

        <Table striped bordered hover className="mt-3">
          <thead>
            <tr>
              <th>#</th>
              <th>TITLE</th>
              <th>DESCRIPTION</th>
              <th>CREATE DATE</th>
              <th>IMAGE</th>
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody>{tr}</tbody>
        </Table>
        <ItemModal
          show={this.state.showModal}
          msg={this.state.message}
          closeModel={this.closeModal}
        />
        <div className="mb-5 page" style={{width:"320"}}>
          <Pagination 
            prevPageText="prev"
            nextPageText="next"
            firstPageText="first"
            lastPageText="last"
            activePage={this.state.activePage}
            itemsCountPerPage={this.state.perPage}
            totalItemsCount={this.state.article.length}
            itemClass="page-item"
            linkClass="page-link"
            onChange={this.handlePageChange.bind(this)}
          />
        </div>
      </Container>
    );
  }
}
