import React, { Component } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import Img from "../Components/Image/placeholder-img.jpg";
export default class ItemRowTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUpdate: true,
      myImage:Img,
    };
  }

  render() {
    return (
      <tr>
        <td>{this.props.article.ID}</td>
        <td>{this.props.article.TITLE}</td>
        <td>{this.props.article.DESCRIPTION}</td>
        <td className="d-flex" style={{ width: "150px" }}>
          {this.props.article.CREATED_DATE===null?"":convertDate(this.props.article.CREATED_DATE)}
        </td>
        <td>
          <img src={this.props.article.IMAGE==null || this.props.article.IMAGE==="string"?this.state.myImage:this.props.article.IMAGE} width="100px" alt="avatar" />
        </td>
        <td
          className="d-flex justify-content-between"
          style={{ width: "230px" }}
        >
          <Button
            variant="primary"
            as={Link}
            to={`/view/${this.props.article.ID}`}
          >
            View
          </Button>
          <Button
            variant="success"
            as={Link}
            to={`/add?isUpdate=${this.state.isUpdate}&&id=${this.props.article.ID}`}
          >
            Edit
          </Button>
          <Button variant="danger" onClick={()=>this.props.onDelete(this.props.article.ID)} >
            Delete
          </Button>
        </td>
      </tr>
    );
  }
}
function convertDate(dateStr) {
  var dateString = dateStr;
  var year = dateString.substring(0, 4);
  var month = dateString.substring(4, 6);
  var day = dateString.substring(6, 8);
  var date = year + "-" + month + "-" + day;
  return date;
}
