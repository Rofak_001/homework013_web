import React, { Component } from "react";
import { Button, Modal } from "react-bootstrap";

export default class ItemModal extends Component {
  render() {
    return (
      <div>
        <Modal show={this.props.show} onHide={this.props.closeModel}>
          <Modal.Header closeButton>
            <Modal.Title>Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h6>{this.props.msg}</h6>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.props.closeModel}>
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
