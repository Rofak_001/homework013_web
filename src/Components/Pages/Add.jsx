import React, { Component } from "react";
import { Container, Form, Button } from "react-bootstrap";
import Img from "../Image/placeholder-img.jpg";
import "../css/style.css";
import Axios from "axios";
import ItemModal from "../ItemModal";
import queryString from "query-string";
export default class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selecledFile: null,
      // preview: Img,
      // Title: "",
      // Des: "",
      ShowModal: false,
      Message: "",
      date: new Date(),
      isUpdate: false,
      id:"",
      article:{
        title:"",
        description:"",
        image:Img,
      }
    };
  }
  // fileSelectedHandler(event) {
  //   var preview = document.getElementById("blah");
  //   var file = event.target.files[0];
  //   var reader = new FileReader();
  //   if (file) {
  //     reader.readAsDataURL(file);
  //   } else {
  //     preview.src = Img;
  //     //   this.setState({ preview: Img });
  //   }
  //   reader.onloadend = () => {
  //     preview.src = reader.result;
  //     this.setState({ selecledFile: reader.result });
  //   };
  //   this.setState({ selecledFile: file });
  // }
  onImageChange = (event) => {
    var preview = document.getElementById("blah");
     preview.src =  URL.createObjectURL(event.target.files[0])
    console.log(event);
    if (event.target.files && event.target.files[0]) {
      this.setState({
          selecledFile: URL.createObjectURL(event.target.files[0])
      });
    }
  };
  upload() {
    let myImage = null;
    //if isUpdate true and we need to check
    //if dont selectfile anything we get defult from api
    //if has selectfile we get image from selectfile  
    if(this.state.isUpdate){
      if(this.state.selecledFile === null){
        myImage=this.state.article.image
      }else{
        myImage = this.state.selecledFile;
      }
    //if isUpdate is false we check 
    //if donst select anything we get image from IMG variable
    // if has selectfile we get image from selectfile
    }else{
      if (this.state.selecledFile === null) {
        myImage = Img;
      } else {
        myImage = this.state.selecledFile;
      }
    }
    if (this.state.article.title !== "" && this.state.article.description !== "") {
      //isUpdate true we will upate article
      if(this.state.isUpdate){
        let article = {
          TITLE: this.state.article.title,
          DESCRIPTION: this.state.article.description,
          IMAGE: myImage,
        };
        Axios.put(`http://110.74.194.124:15011/v1/api/articles/${this.state.id}`, article).then(
          (res) => {
            console.log(res.data.MESSAGE);
  
            this.setState({
              Message: res.data.MESSAGE,
              ShowModal: true,
            });
          }
        );
        //isUpdate fales we wiil add new article
      }else{
        let article = {
          TITLE: this.state.article.title,
          DESCRIPTION: this.state.article.description,
          IMAGE: myImage,
        };
        Axios.post("http://110.74.194.124:15011/v1/api/articles", article).then(
          (res) => {
            console.log(res.data.MESSAGE);
  
            this.setState({
              Message: res.data.MESSAGE,
              ShowModal: true,
            });
          }
        );
      }
      document.getElementById("Title").classList.remove("STitle");
      document.getElementById("Title").classList.add("WTitle");
      document.getElementById("Des").classList.remove("SDes");
      document.getElementById("Des").classList.add("WDes");
    } else if (this.state.article.title === "" && this.state.article.description === "") {
      document.getElementById("Title").classList.remove("WTitle");
      document.getElementById("Title").classList.add("STitle");
      document.getElementById("Des").classList.remove("WDes");
      document.getElementById("Des").classList.add("SDes");
    } else if (this.state.article.title === "" && this.state.article.description !== "") {
      document.getElementById("Title").classList.remove("WTitle");
      document.getElementById("Title").classList.add("STitle");
    } else if (this.state.article.description === "" && this.state.article.title !== "") {
      document.getElementById("Des").classList.remove("WDes");
      document.getElementById("Des").classList.add("SDes");
    }
  }
  getTite(event) {
    //  console.log(event.target.value);
    // this.setState({ Title: event.target.value });
    this.setState({article:{
      ...this.state.article,
      title:event.target.value,
    }})
    document.getElementById("Title").classList.remove("STitle");
    document.getElementById("Title").classList.add("WTitle");
  }
  getDes(event) {
    // this.setState({ Des: event.target.value });
    this.setState({article:{
      ...this.state.article,
      description:event.target.value,
    }})
    document.getElementById("Des").classList.remove("SDes");
    document.getElementById("Des").classList.add("WDes");
  }
  closeModal = () => {
    this.setState({ ShowModal: false });
    this.props.history.push("/");
  };
  componentWillMount(){
    const values = queryString.parse(this.props.location.search);
    if(values.isUpdate){
    this.setState({
       isUpdate: values.isUpdate,
       id:values.id
       });
      Axios.get(`http://110.74.194.124:15011/v1/api/articles/${values.id}`).then((res)=>{
        console.log(res.data.DATA.TITLE);
        this.setState({article:{
          ...this.state.article,
          title:res.data.DATA.TITLE,
          description:res.data.DATA.DESCRIPTION,
          image:res.data.DATA.IMAGE,
        }})
      })
    }
  }
  render() {
    return (
      <Container className="d-flex justify-content-between mt-3">
        <div className="row w-100">
          <div className="col-8">
            <div style={{ textAlign: "center" }}>
              <h5>
                {this.state.isUpdate ? "Update Article" : "Add New Article"}
              </h5>
            </div>
            <Form.Group>
              <Form.Label>TITLE</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter title"
                onChange={this.getTite.bind(this)}
                value={this.state.isUpdate?`${this.state.article.title}`:`${this.state.article.title}`}
              />
              <p className="warnnig WTitle" id="Title">
                *Title can not be blank*
              </p>
            </Form.Group>
            <Form.Group>
              <Form.Label>DESCRIPTION</Form.Label>
              <textarea
                className="form-control"
                rows="5"
                id="comment"
                placeholder="Enter Description"
                onChange={this.getDes.bind(this)}
                value={this.state.isUpdate?`${this.state.article.description}`:`${this.state.article.description}`}
              ></textarea>
              <p className="warnnig WDes" id="Des">
                *Description can not be blank*
              </p>
            </Form.Group>
            <Form.Group>
              <Button onClick={this.upload.bind(this)}>{this.state.isUpdate?"Update":"Add"}</Button>
            </Form.Group>
          </div>
          <div className="col-4">
            <div>
              <img
                src={this.state.isUpdate?`${this.state.article.image}`:`${this.state.article.image}`}
                alt="Avatar"
                width="100%"
                height="250px"
                id="blah"
              />
            </div>
            <div className="text-center mt-3">
              <input
                type="file"
                onChange={this.onImageChange.bind(this)}
              />
            </div>
          </div>
        </div>
        <ItemModal
          show={this.state.ShowModal}
          msg={this.state.Message}
          closeModel={this.closeModal.bind(this)}
        />
      </Container>
    );
  }
}
