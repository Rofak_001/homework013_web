import { Container } from "react-bootstrap";
import Img from "../Image/placeholder-img.jpg";
import React, { Component } from "react";
import Axios from "axios";
export default class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      des: "",
      img: "",
    };
  }
  componentWillMount() {
    const id = this.props.match.params.id;
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(
      (res) => {
        console.log(res);
        console.log(res.data.DATA.IMAGE)
        this.setState({
          title: res.data.DATA.TITLE,
          des:res.data.DATA.DESCRIPTION,        
        });
        if(res.data.DATA.IMAGE===null || res.data.DATA.IMAGE==="string"){
            this.setState({img:Img})
        }else{
            this.setState({img:res.data.DATA.IMAGE})
        }
      }
    );
   
  }
  render() {
    return (
      <Container>
        <h3>Article</h3>
        <div className="row">
          <div className="col-4">
            <img src={this.state.img} alt="Avatar" width="100%" />
          </div>
          <div className="col-8">
            <h5>{this.state.title}</h5>
            <h6>
                {this.state.des}
            </h6>
          </div>
        </div>
      </Container>
    );
  }
}
